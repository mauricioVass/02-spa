import { Component, OnInit, Input ,Output, EventEmitter} from '@angular/core';
import { Router } from "@angular/router";
@Component({
  selector: 'app-heroe-tarjeta',
  templateUrl: './heroe-tarjeta.component.html',
  styleUrls: ['./heroe-tarjeta.component.css']
})
export class HeroeTarjetaComponent implements OnInit {

  @Input() h:any={};// recibir h desde su componente padre, daben llamarse igual
  @Input() index:number;

  @Output() heroeSeleccionado: EventEmitter<number>;// porque va a emitir el tipo que usa index
  
  constructor(private router:Router) { 
    this.heroeSeleccionado = new EventEmitter();// es necesario inicializar Event
  }

  ngOnInit() {
  }

  verHeroe(){

    this.router.navigate(['/heroe',this.index]);

    //this.heroeSeleccionado.emit(this.index); //ejemplo para output
  }
}
