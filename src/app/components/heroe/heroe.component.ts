import { Component, OnInit } from '@angular/core';
import{ActivatedRoute}from '@angular/router';
import{HeroesService, heroeInterface} from '../../servicios/heroes.service';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css']
})
export class HeroeComponent implements OnInit {

    heroe:heroeInterface;
  constructor(private ar:ActivatedRoute,private _heroesServices:HeroesService) { 

    this.ar.params.subscribe( params =>{
      this.heroe=_heroesServices.getHeroe(params['id']);
      console.log(params['id']);
    }


    )
  }

  ngOnInit() {
  }

}
