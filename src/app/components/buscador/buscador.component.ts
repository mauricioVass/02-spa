import { Component, OnInit } from '@angular/core';
import{ActivatedRoute,Router}from '@angular/router';

import{HeroesService} from '../../servicios/heroes.service';
@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {

  //let no se utiliza cuando es un atributo
  heroes:any[]=[];
  termino:String;
  constructor(private activatedRuote:ActivatedRoute,
              private _heroesServices:HeroesService,
              private router:Router) { }
  
  ngOnInit() {
    
    this.activatedRuote.params.subscribe( params =>{
      this.termino=params['termino'];
      this.heroes=this._heroesServices.buscarHeroes(params['termino']);
      //heroes es un array, en html hay que utilizar ngFor
    }  )
  }

  verHeroe(idx:number){

    this.router.navigate(['/heroe',idx]);
  }

}
